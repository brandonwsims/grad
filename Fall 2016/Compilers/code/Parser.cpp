#include "Parser.h"
#include "Token.h"

Parser::Parser(Source in) {
    this->in = in;
    this->scanner = new Scanner(in);
}

~Parser::Parser() {
    delete scanner;
}

// Essentially the compile function
void Parser::parse() {
    Token t = scanner.nextToken();
    while(t.value > 0) {
        cout << t;
        t = scanner.nextToken();
    }
    cout << t;
}