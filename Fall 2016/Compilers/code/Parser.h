#ifndef PARSER_H
#define PARSER_H

#include "Source.h";
#include <iostream>

using namespace::std;

class Parser {
    public:
        Parser(Source in);
        ~Parser();
        void parse();
    private:
        Scanner scanner;
        Source in;
};

#endif