#ifndef __SOURCE_H__
#define __SOURCE_H__

#define DEFAULTTAB 8

class Source {
	public:
		Source(char* filename, int tabwidth=DEFAULTTAB);
		Source(char* filename);
		Source(istream in, int tabwidth=DEFAULTTAB);
		Source(istream in);
		~Source();
	private:
		char* filename;
		istream input;
		int tabwidth; 
}