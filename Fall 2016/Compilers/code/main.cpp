#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>

using namespace std;

string banner = "C-- Compiler, v0.0.1";
ifstream in;
int tabSize;
int exitCode = 0;
char* filename;

const char DELIM = ':';

// For processing command line arguments
int getOptions(int argc, char** argv);

int main(int argc, char** argv) {
    // Process options
    exitCode = getOptions(argc, argv);
    return exitCode;	
}

int getOptions(int argc, char** argv) {
    // Exit code to keep track of compiler state after reading arguements
    int exitCode = 0;
    // For pointing to option and arguement within argv
    char* opt;
    char* arg;
    // Flag set true if stdin is specified so compiler knows to ignore filenames
    bool stdin = false;
    // While the end of the options haven't been exhausted and a filename hasn't been encountered
    for(int i = 1; argv[i] != NULL && argv[i][0] == '-'; i++) {
        opt = strtok(&(argv[i][1]), &DELIM);
        arg = strtok(NULL, "");
        if(opt == NULL) {
            // Only recieved a dash
            exitCode = 100;
        } else if(strcmp(opt, "-help") == 0 || strcmp(opt, "h") == 0 || strcmp(opt, "?") == 0) {
            // Print help -- Do set widths with these later
            cout << "-----------------------------------------------------------------------------------" << endl;
            cout << "Usage:\tcmmc <flags> <filename>" << endl;
            cout << "Note:\tfilename is marked as optional due to the -stdin flag below" << endl << endl;
            cout << "Available Commands" << endl;
            cout << "-h --help -?\t\tDisplays information currently shown" << endl;
            cout << "-t --tabwidth <#>\tSpecify single digit numer of spaces to represent a tab" << endl;
            cout << "-v --version\t\tDisplay installed version of cmmc" << endl;
            cout << "-- -stdin\t\tSpecify to read stdin from command line for debugging" << endl;
            cout << "-----------------------------------------------------------------------------------" << endl;
        } else if(strcmp(opt, "-tabwidth") == 0 || strcmp(opt, "t") == 0) {
            // If a number was given
            if(atoi(strtok(argv[i+1], &DELIM))) {
                tabSize = atoi(strtok(argv[i+1], &DELIM));
                if(tabSize <= 0) {
                    // throw InvalidTabSize and set exitCode;
                }
                cout << "tabwidth: " << tabSize << endl;
                // Make sure the number isn't read as the next flag of the loops iteration
                i++;
            }
        } else if(strcmp(opt, "-version") == 0 || strcmp(opt, "v") == 0) {
            // Print version
            cout << banner << endl;
        } else if(strcmp(opt, "-stdin") == 0 || strcmp(opt, "-") == 0) {
            // Set stdin flag so filename will be ignored
            stdin = true;
            // For processing user input from stdin
            string input = "";
            string token = "";
            // Read from stdin
            cout << "Enter input and terminate with \\end on a line by its self" << endl;
            while(!cin.eof() && token != "\\end") {
                input += token + '\n';
                getline(cin, token);
            }
            cout << input;
        } else {
            cout << "Error:\t unknown command line option: " << opt << endl;
            cout << "Usage:\tcmmc <flags> <filename>" << endl;
            cout << "For a list of available commands run again with -h" << endl;
        }
    }
    // Set filename to last argument and make sure filename is last option
    if(!stdin && argc > 1) {
        filename = argv[argc - 1];
        cout << filename << endl;
    }
    return exitCode;
}